'use strict';
~ function () {
    var $ = TweenMax;

    window.init = function () {
 
        MorphSVGPlugin.convertToPath("rect");
        MorphSVGPlugin.convertToPath("circle");
        MorphSVGPlugin.convertToPath("polygon");

        var tl = new TimelineMax();
        tl.to("#rect", 1, {morphSVG:"#circle"}, "+=1");
        tl.to("#rect", 1, {morphSVG:"#star"}, "+=1");
        tl.to("#rect", 1, {morphSVG:"#word"}, "+=1");


    }
   
}();
