var container,
bgExit,
border,
Animation_tl,
Image_tl;
var Logo_tl;

setUp();

function setUp () {
    console.log("----- setUp")
    Assign_DOM_Elements ();
    CreateAnimations ();
    addEventListeners();
    reset ();
    Play ();
    
  // document.getElementById('container_dc').style.display = "block";
  
}

 function Assign_DOM_Elements() {
    console.log("----- Assign_DOM_Elements")
  	container = document.getElementById('container_dc');
	bgExit = document.getElementById('background_exit_dc');	
    text_1_1 = document.getElementById('text_1_1'); 
    text_1_2 = document.getElementById('text_1_2'); 
    text_1_3 = document.getElementById('text_1_3'); 
    text_2_1 = document.getElementById('text_2_1'); 
    text_2_2 = document.getElementById('text_2_2'); 
    img1 = document.getElementById('img1'); 
    img2 = document.getElementById('img2'); 
    img3 = document.getElementById('img3'); 

    black_panel = document.getElementById('black_panel'); 


    //////// YT LOGO //////////////////
    YT_Logo = document.getElementById('YT_Logo'); 
    YT_Icon = document.getElementById('YT_Icon'); 
    YTMusicText_Container = document.getElementById('YTMusicText_Container'); 
    YTMusicText = document.getElementById('YTMusicText');
    Ring = document.getElementById('Ring');
    Circle = document.getElementById('Circle');
    //////// YT LOGO //////////////////
}


function reset () {
    console.log("----- reset")
    Animation_tl.kill();
    Animation_tl.gotoAndStop(0);

    Image_tl.kill();
    Image_tl.gotoAndStop(0);

    TweenLite.set(['#text_1_1', '#text_1_2', '#text_1_3'], {opacity:0, y:20});
    TweenLite.set(['#text_2_1','#text_2_2'], {opacity:0, y:20});
    TweenLite.set('#img1', {opacity:1, scale:1, rotationZ:0.001});
    TweenLite.set('#img2', {opacity:0, scale:1, rotationZ:0.001});
    TweenLite.set('#img3', {opacity:0, scale:1, rotationZ:0.001});


    TweenLite.set('#black_panel', {y:0, scaleY:1, opacity: 1, transformOrigin:'0% 100%'});


    //////// YT LOGO //////////////////
    Logo_tl.kill();
    Logo_tl.gotoAndStop(0);
    TweenLite.set('#YT_Logo', {x:56, y:99, scale:0.6});
    TweenLite.set('#Ring', {opacity:0});
    TweenLite.set('#Circle', {opacity:0});
    //////// YT LOGO //////////////////
}

function CreateAnimations () {

	console.log("----- CreateAnimations")

    Image_tl = new TimelineMax({paused:true, repeat:0, repeatDelay:2});
    Image_tl.timeScale(1);
    Image_tl.to('#img1', 6.5, {scale:1.2, ease: Power0.easeNone});


    Image_tl.to('#img2', 0.5, {delay:-1.5, opacity:1, ease: Power0.easeNone});
    Image_tl.to('#img2', 3.5, {delay:-1.5, scale:1.2, ease: Power0.easeNone});



    Image_tl.to('#img3', 1, {delay:-1.8, opacity:1, ease: Power0.easeNone});
    Image_tl.to('#img3', 5, {delay:-1.5, scale:1.2, ease: Power0.easeNone});



    Animation_tl = new TimelineMax({paused:true, repeat:0, repeatDelay:2});
    Animation_tl.timeScale(1);
    Animation_tl.to('#black_panel', 0.8, {delay:0.5, y:100, scaleY:0, opacity: 0, transformOrigin:'0% 100%', ease: Power2.easeIn, onStart:playImages});
    Animation_tl.to('#YT_Logo', 0.8, {delay:-0.8, y:190, ease: Power2.easeIn});

    Animation_tl.to('#text_1_1', 1, {delay:-0.2, opacity:1, y:0, ease: Power2.easeOut});
    Animation_tl.to('#text_1_2', 1, {delay:-0.8, opacity:1, y:0, ease: Power2.easeOut});
    Animation_tl.to('#text_1_3', 1, {delay:-0.8, opacity:1, y:0, ease: Power2.easeOut});


    Animation_tl.to('#text_1_1', 1, {delay:2.5, opacity:0, ease: Power2.easeOut});
    Animation_tl.to('#text_1_2', 1, {delay:-1, opacity:0, ease: Power2.easeOut});
    Animation_tl.to('#text_1_3', 1, {delay:-1, opacity:0, ease: Power2.easeOut});


    Animation_tl.to('#text_2_1', 1, {delay:-0, opacity:1, y:0, ease: Power2.easeOut});
    Animation_tl.to('#text_2_2', 1, {delay:0.3, opacity:1, y:0, ease: Power2.easeOut});


    Animation_tl.to('#text_2_1', 1, {delay:1.5, opacity:0, ease: Power2.easeOut});
    Animation_tl.to('#text_2_2', 1, {delay:-1, opacity:0, ease: Power2.easeOut});


    Animation_tl.to('#YT_Logo', 0.8, {delay:-0, y:99, ease: Power2.easeOut});
    Animation_tl.to('#black_panel', 0.8, {delay:-0.8, y:0, scaleY:1, opacity: 1, transformOrigin:'0% 100%', ease: Power2.easeOut, onComplete:playLogo});
    
    



    //// entrance
    //Animation_tl.to(phone, 1,  {y:47, ease: Power2.easeOut});


    Logo_tl = new TimelineMax({paused:true});
    Logo_tl.to('#Oval', 0.8, {morphSVG:Circle, ease: Power2.easeInOut});
    Logo_tl.to('#Ring', 0.8, {delay:-0.8, opacity:1, ease: Power2.easeInOut});
    Logo_tl.to('#YT_Icon', 0.8, {delay:-0.8, rotation:360, x:108, scale:2.5, ease: Power2.easeInOut});
    Logo_tl.to('#YTMusicText', 0.8, {delay:-0.8, x:-300, ease: Power2.easeInOut});
    Logo_tl.to('#YTMusicText_Container', 0.8, {delay:-0.8, x:100, ease: Power2.easeInOut});
    //Logo_tl.to(YT_Icon, 0.2, {delay:0.2, y:20, ease: Power1.easeIn});
    //Logo_tl.to(YT_Icon, 0.5, {delay:0, y:-20, ease: Power1.easeOut});

}



function pauseAnimation () {
    Animation_tl.pause();
}

function playImages () {
    Image_tl.gotoAndPlay(0);
}

function playLogo () {
    Logo_tl.gotoAndPlay(0);
}

function addEventListeners () {
    console.log("----- addEventListeners")
    // bgExit.addEventListener('click', bgExitHandler, false);	
}

function Play () {
	console.log("----- Play")
    Animation_tl.gotoAndPlay(0);
}

function CheckLoops () {   
	console.log("----- CheckLoops")
	if (PlayCount == MaxLoops) {
		Animation_tl.pause();
		console.log("----- End")
	}
}

bgExitHandler = function(e) {
    //Call Exits
    reset();
    Play();
    return;
    window.open(window.clickTag, "_blank");
    //Enabler.exit('click');    
}